# Extra-Ascension-Slots-6
Stellaris Mod Extra Ascension Slots (6)

##  Description: ##
A simple mod which adds 6 extra Ascension Trait slots and a repeatable tech to get them.

## Features: ##
Adds 6 Extra Ascension Trait Slots
Adds a repeatable Tech to get the Ascension Slots

## Incompatibilities: ##
Mods that modify the Traditions View or the max allowed number of Ascension Perks.

---

#### Support: ####
*https://jira.meridanas.me/servicedesk/customer/portal/5*

#### Direct Download: ####
*https://bitbucket.org/meridanas/extra-ascension-slots-6/downloads/*